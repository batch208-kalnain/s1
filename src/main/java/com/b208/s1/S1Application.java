package com.b208.s1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication
@RestController
// It will tell Springboot that this application will function as an endpoint handling web request.
public class S1Application {
	// Annotation in Java Springboot marks classes for their intended functionalities.
	// Springboot upon startup scans for classes and assign behaviors based on their annotation.
	public static void main(String[] args) {
		SpringApplication.run(S1Application.class, args);
	}

	// GetMapping() will map a route or an endpoint to access or run our method.
	// When http://localhost:8080/hello is accessed from a client, we will be able to run the hi() method.
	@GetMapping("/hello")
	public String hello(){
		return "Hi from our Java Springboot app!";
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value="name",defaultValue = "Doe")String nameParameter){
		// Pass data through the url using our string query
		// http://localhost:8080/hi?name=Doe
		// We retrieve the value of name from our URL.
		// Default value is the fallback value when query/request param is empty.
		return "Hi, my name is " + nameParameter;
	}

	// 2 ways of passing data through the URL by using Java Springboot.
	// Query String using Request Params - Directly passing data into the url and getting the data.
	// Path Variable is much more similar to ExpressJS's req.params. Mostly used for passing id.

	@GetMapping("/myFavoriteFood")
	public String myFavoriteFood(@RequestParam(value="food",defaultValue = "Ice Cream")String foodParameter){
		return "Hi, my favorite food is " + foodParameter;
	}

	// Passing of data using Path Variable
	@GetMapping("/greeting/{name}")
	public String greeting(@PathVariable("name")String nameParams){
		return "Hello " + nameParams;
	}
	ArrayList<String> enrollees =  new ArrayList<>();
	@GetMapping("/enroll")
	public String enroll(@RequestParam(value="name")String nameParams){
		if(nameParams.isEmpty()){
			return "Username parameter is required";
		} else {
			enrollees.add(nameParams);
			return "Welcome " + nameParams + "!";
		}
	}

	@GetMapping("/getEnrollees")
	public ArrayList<String> getEnrolles(){
		return enrollees;
	}

	@GetMapping("/courses/{id}")
	public String courses(@PathVariable("id")String idParams){
		switch (idParams){
			case "java101":
				return "Name:Java101, Schedule:M-F 08:00AM - 05:00PM, Price:3000";
			case "sql101":
				return "Name:SQL101, Schedule:M-F 08:00AM - 05:00PM, Price:2000";
			case "jsoop101":
				return "Name:JS OOP 101, Schedule:M-F 08:00AM - 05:00PM, Price:10000";
			default:
				return "Course cannot be found";
		}
	}
}
